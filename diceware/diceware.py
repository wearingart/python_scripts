from random import randint

diceDict = {}
i = 0
j = 0
diceRoll = ''
newPassList = []
newPass = ''
allRolls = []

with open('diceware.wordlist.asc', 'r') as f:
    for line in f:
        splitLine = line.split()
        diceDict[int(splitLine[0])] = splitLine[1]

while i < 5:
    while j < 5:
        diceRoll = diceRoll + str(randint(1,6))
        j = j + 1
    newPassList.append(diceDict[int(diceRoll)])
    diceRoll = ''
    j = 0
    i = i + 1

print(diceRoll)
print(''.join(newPassList))
